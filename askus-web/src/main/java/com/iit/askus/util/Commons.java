package com.iit.askus.util;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.PasswordEncoder;

//Singleton Object for Common properties and methods
public class Commons {

    private static Commons commonsInstance;
    public static String BIO_SCIENCE = "Biology";
    public static String PHYSICS = "Physics";
    public static String CHEMISTRY = "Chemistry";

    private Commons() {
    }

    public static Commons getInstance() {
        if (null == commonsInstance) {
            commonsInstance = new Commons();
        }
        return commonsInstance;
    }

    /*Converts the Date String retrieved from the Form to java.sql.Date object*/
    public Date convertStr2Date(String date) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
        return new Date(simpleDateFormat.parse(date).getTime());
    }

    /*Converts java.sql.Date object back to a String*/
    public String convertDate2Str(Date date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date newDate = sdf.parse(date.toString());
        sdf.applyPattern("MM/dd/yyyy");
        return sdf.format(newDate);
    }

    /*Generates MD5 Hash for a given string using Spring Security Password Encoder methods*/
    public String generateMD5(String plainString){
        PasswordEncoder encoder = new Md5PasswordEncoder();
        String hashedPass = encoder.encodePassword(plainString, null);
        return hashedPass;
    }
}