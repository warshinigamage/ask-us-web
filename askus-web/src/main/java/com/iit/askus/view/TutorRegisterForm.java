package com.iit.askus.view;

import java.util.List;

public class TutorRegisterForm {

    private String userName;
    private String password;
    private String firstName;
    private String lastName;
    private String birthDate;
    private String emailAddress;
    private String nicNo;
    private String gender;
    private String skypeId;
    private String qualifiedSubject;
    private String educationalQualifications;
    private String professionalQualifications;
    private List<String> genderList;
    private List<String> qualifiedSubjectList;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getNicNo() {
        return nicNo;
    }

    public void setNicNo(String nicNo) {
        this.nicNo = nicNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<String> getGenderList() {
        return genderList;
    }

    public void setGenderList(List<String> genderList) {
        this.genderList = genderList;
    }

    public String getSkypeId() {
        return skypeId;
    }

    public void setSkypeId(String skypeId) {
        this.skypeId = skypeId;
    }

    public String getQualifiedSubject() {
        return qualifiedSubject;
    }

    public void setQualifiedSubject(String qualifiedSubject) {
        this.qualifiedSubject = qualifiedSubject;
    }

    public String getEducationalQualifications() {
        return educationalQualifications;
    }

    public void setEducationalQualifications(String educationalQualifications) {
        this.educationalQualifications = educationalQualifications;
    }

    public String getProfessionalQualifications() {
        return professionalQualifications;
    }

    public void setProfessionalQualifications(String professionalQualifications) {
        this.professionalQualifications = professionalQualifications;
    }

    public List<String> getQualifiedSubjectList() {
        return qualifiedSubjectList;
    }

    public void setQualifiedSubjectList(List<String> qualifiedSubjectList) {
        this.qualifiedSubjectList = qualifiedSubjectList;
    }
}
