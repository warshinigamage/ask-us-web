package com.iit.askus.service;

import com.iit.askus.dao.impl.SubjectDAO;
import com.iit.askus.model.Subject;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class SubjectService {

    @Autowired
    private SubjectDAO subjectDAO;

    //Calls subject DAO to get all subjects
    public List<Subject> getAllSubjects() {
        return subjectDAO.getAllSubjects();
    }

    //Calls subject DAO to get a subject object using subject id
    public Subject getSubjectBySubjectId(long subjectId) {
        return subjectDAO.getSubjectBySubjectId(subjectId);
    }

}
