package com.iit.askus.service;

import com.iit.askus.dao.impl.LessonDAO;
import com.iit.askus.model.Lesson;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class LessonService {

    @Autowired
    private LessonDAO lessonDAO;

    //Calls Lesson DAO to get list of lessons using subject id
    public List<Lesson> getLessonsBySubjectId(long subjectId) {
        return lessonDAO.getLessonsBySubjectId(subjectId);
    }

    // Calls Lesson DAO to get a Lesson using lesson id
    public Lesson getLessonByLessonId(long lessonId) {
        return lessonDAO.getLessonByLessonId(lessonId);
    }
}