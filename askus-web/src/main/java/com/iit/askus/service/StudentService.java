package com.iit.askus.service;

import com.iit.askus.dao.impl.UserDAO;
import com.iit.askus.model.Gender;
import com.iit.askus.model.Student;
import com.iit.askus.model.User;
import com.iit.askus.model.UserGuardGroup;
import com.iit.askus.util.Commons;
import com.iit.askus.view.StudentRegisterForm;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class StudentService {

    @Autowired
    private UserDAO userDAO;

    // Calls the UserDAO to save the student to the database
    public void saveStudent(StudentRegisterForm studentRegisterForm) throws ParseException {
        userDAO.saveUser(createStudentModel(studentRegisterForm));
    }

    // Creates a student registration form
    public StudentRegisterForm createStudentRegistrationForm() {
        StudentRegisterForm studentRegisterForm = new StudentRegisterForm();
        List genders = new ArrayList<String>();
        for (Gender gender : Gender.values()){
            genders.add(gender.getValue());
        }
        studentRegisterForm.setGenderList(genders);

        List examYears = new ArrayList<String>();
        examYears.add("2014");
        examYears.add("2015");
        examYears.add("2016");
        studentRegisterForm.setExamYearList(examYears);
        return studentRegisterForm;
    }

    //Creates a User model object using the student register form details
    private User createStudentModel(StudentRegisterForm studentRegisterForm) throws ParseException {
        UserGuardGroup userGuardGroupStudent = new UserGuardGroup();
        userGuardGroupStudent.setUserGroupId(3);
        userGuardGroupStudent.setUserGroupName("ROLE_STUDENT_GROUP");

        User user = new User();
        user.setUserName(studentRegisterForm.getUserName());
        user.setPassword(Commons.getInstance().generateMD5(studentRegisterForm.getPassword()));
        user.setFirstName(studentRegisterForm.getFirstName());
        user.setLastName(studentRegisterForm.getLastName());
        user.setNicNo(studentRegisterForm.getNicNo());
        user.setEmail(studentRegisterForm.getEmailAddress());
        user.setBirthDate(Commons.getInstance().convertStr2Date(studentRegisterForm.getBirthDate()));
        user.setGender(Gender.M);
        user.setEnabled(true);

        Student student = new Student();
        student.setUser(user);
        student.setSchool(studentRegisterForm.getSchool());
        student.setExamYear(studentRegisterForm.getExamYear());

        user.setStudent(student);
        user.setUserGuardGroup(userGuardGroupStudent);

        return user;
    }
}
