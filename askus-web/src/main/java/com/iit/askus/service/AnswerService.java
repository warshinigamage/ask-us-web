package com.iit.askus.service;

import com.iit.askus.dao.impl.AnswerDAO;
import com.iit.askus.dao.impl.QuestionDAO;
import com.iit.askus.dao.impl.TutorDAO;
import com.iit.askus.model.Answer;
import com.iit.askus.model.Question;
import com.iit.askus.model.Tutor;
import com.iit.askus.view.AnswerForm;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class AnswerService {

    @Autowired
    private AnswerDAO answerDAO;

    @Autowired
    private TutorDAO tutorDAO;

    @Autowired
    private QuestionDAO questionDAO;

    //Calls the Answer DAO to save the Answer
    public void saveAnswer(AnswerForm answerForm) {
        answerDAO.saveAnswer(createAnswerModel(answerForm));
    }

    //Cals the Answer DAO to get the list of answers using question id
    public List<Answer> getAnswersByQuestionId(long questionId) {
       return answerDAO.getAnswersByQuestionId(questionId);
    }

    // Creates the Answer Form
    public AnswerForm createAnswerForm(String userId) {
        AnswerForm answerForm = new AnswerForm();
        answerForm.setTutorId(userId);
        return answerForm;
    }

    //Creates the Answer model object using the values in answer form
    private Answer createAnswerModel(AnswerForm answerForm) {
        Answer answer = new Answer();

        Tutor tutor = tutorDAO.getTutorByUserId(Integer.parseInt(answerForm.getTutorId()));
        Question question = questionDAO.getQuestionByQuestionId(Long.parseLong(answerForm.getQuestionId()));

        answer.setAnswer(answerForm.getAnswer());;
        answer.setTutor(tutor);
        answer.setQuestion(question);

        return answer;
    }
}
