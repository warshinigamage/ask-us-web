package com.iit.askus.service;

import com.iit.askus.dao.impl.LessonDAO;
import com.iit.askus.dao.impl.QuestionDAO;
import com.iit.askus.dao.impl.StudentDAO;
import com.iit.askus.model.Lesson;
import com.iit.askus.model.Question;
import com.iit.askus.model.Student;
import com.iit.askus.util.Commons;
import com.iit.askus.view.QuestionForm;
import com.iit.askus.view.SearchForm;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class QuestionService {

    @Autowired
    private QuestionDAO questionDAO;

    @Autowired
    private StudentDAO studentDAO;

    @Autowired
    private LessonDAO lessonDAO;

    //Calls the Question DAO to save the question to the database
    public void saveQuestion(QuestionForm questionForm) {
        questionDAO.saveQuestion(createQuestionModel(questionForm));
    }

    //Calls the Question DAO to get a list of questions using lesson id
    public List<Question> getQuestionsByLessonId(long lessonId) {
        return questionDAO.getQuestionsByLessonId(lessonId);
    }

    //Calls the Question DAO to get a Question using question id
    public Question getQuestionByQuestionId(long questionId) {
        return questionDAO.getQuestionByQuestionId(questionId);
    }

    //Creates the question form
    public QuestionForm createQuestionForm(String userId) {
        QuestionForm questionForm = new QuestionForm();
        questionForm.setStudentId(userId);
        return questionForm;
    }

    //Creates a Question model object using question form
    private Question createQuestionModel(QuestionForm questionForm) {
        Question question = new Question();

        Student student = studentDAO.getStudentByUserId(Integer.parseInt(questionForm.getStudentId()));
        Lesson lesson = lessonDAO.getLessonByLessonId(Long.parseLong(questionForm.getLessonId()));
        question.setQuestion(questionForm.getQuestion());
        question.setStudent(student);
        question.setLesson(lesson);

        return question;
    }

    //Creates the Search Form
    public SearchForm createSearchForm() {
        System.out.println("Creating Search Form >>>");
        SearchForm searchForm = new SearchForm();
        List<String> subjects = new ArrayList<String>();
        subjects.add("Any");
        subjects.add(Commons.getInstance().BIO_SCIENCE);
        subjects.add(Commons.getInstance().CHEMISTRY);
        subjects.add(Commons.getInstance().PHYSICS);

        searchForm.setSubjectList(subjects);
        return searchForm;
    }

    //Calls the question dao to search a question using the searchtext and the subject id
    public List<Question> searchQuestion(String searchText, String subjectId) {
        return questionDAO.searchQuestion(searchText, subjectId);
    }

}
