package com.iit.askus.service;

import com.iit.askus.dao.impl.TutorDAO;
import com.iit.askus.dao.impl.UserDAO;
import com.iit.askus.model.*;
import com.iit.askus.util.Commons;
import com.iit.askus.view.TutorRegisterForm;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class TutorService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private TutorDAO tutorDAO;


    //Calls the UserDAO to save a user to the database
    public void saveTutor(TutorRegisterForm tutorRegisterForm) throws ParseException  {
        userDAO.saveUser(createTutorModel(tutorRegisterForm));
    }

    //Creates a Tutor registration form
    public TutorRegisterForm createTutorRegistrationForm() {
        TutorRegisterForm tutorRegisterForm = new TutorRegisterForm();
        List genders = new ArrayList<String>();
        for (Gender gender : Gender.values()){
            genders.add(gender.getValue());
        }
        tutorRegisterForm.setGenderList(genders);

        List qualifiedSubjects = new ArrayList<String>();
        qualifiedSubjects.add(Commons.getInstance().BIO_SCIENCE);
        qualifiedSubjects.add(Commons.getInstance().PHYSICS);
        qualifiedSubjects.add(Commons.getInstance().CHEMISTRY);
        tutorRegisterForm.setQualifiedSubjectList(qualifiedSubjects);

        return tutorRegisterForm;
    }

    //Creates a User model object using the tutor register form details
    private User createTutorModel(TutorRegisterForm tutorRegisterForm) throws ParseException {
        UserGuardGroup userGuardGroupStudent = new UserGuardGroup();
        userGuardGroupStudent.setUserGroupId(2);
        userGuardGroupStudent.setUserGroupName("ROLE_TUTOR_GROUP");

        User user = new User();
        user.setUserName(tutorRegisterForm.getUserName());
        user.setPassword(Commons.getInstance().generateMD5(tutorRegisterForm.getPassword()));
        user.setFirstName(tutorRegisterForm.getFirstName());
        user.setLastName(tutorRegisterForm.getLastName());
        user.setNicNo(tutorRegisterForm.getNicNo());
        user.setEmail(tutorRegisterForm.getEmailAddress());
        user.setBirthDate(Commons.getInstance().convertStr2Date(tutorRegisterForm.getBirthDate()));
        user.setGender(Gender.M);
        user.setEnabled(false);

        Tutor tutor = new Tutor();
        tutor.setUser(user);
        tutor.setSkypeId(tutorRegisterForm.getSkypeId());
        tutor.setQualifiedSubject(getQualifiedSubject(tutorRegisterForm.getQualifiedSubject()));
        tutor.setEducationalQualifications(tutorRegisterForm.getEducationalQualifications());
        tutor.setProfessionalQualifications(tutorRegisterForm.getProfessionalQualifications());
        tutor.setTutorStatus(TutorStatus.PENDING_APPROVAL);

        user.setTutor(tutor);
        user.setUserGuardGroup(userGuardGroupStudent);

        return user;
    }

    //Calls the TutorDAO to get a list of tutors in PENDING_APPROVAL status
    public List<Tutor> getPendingTutors() {
        return tutorDAO.getPendingTutors();
    }

    //Calls the TutorDAO to update the Tutor status
    public void approveTutor(int userId){
        Tutor tutor = tutorDAO.getTutorByUserId(userId);
        tutor.setTutorStatus(TutorStatus.APPROVED);
        User user = tutor.getUser();
        user.setEnabled(true);
        user.setTutor(tutor);

        tutorDAO.update(user);
    }

    //Returns a subject that tutor qualifes in
    private Subject getQualifiedSubject(String subjectName) {
        Subject subject = new Subject();

        if(subjectName.equals(Commons.getInstance().BIO_SCIENCE)) subject.setSubjectId(1);
        else if (subjectName.equals(Commons.getInstance().PHYSICS)) subject.setSubjectId(2);
        else subject.setSubjectId(3);

        subject.setSubjectName(subjectName);
        return subject;
    }
}
