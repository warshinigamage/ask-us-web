package com.iit.askus.service;

import com.iit.askus.dao.impl.UserDAO;
import com.iit.askus.model.User;
import org.springframework.beans.factory.annotation.Autowired;

public class UserService {

    @Autowired
    private UserDAO userDAO;

    //Calls User DAO to get a user by user id
    public User getUserByUserId(String userId) {
        return userDAO.getUserById(Integer.parseInt(userId));
    }

    // Calls User DAO to get a user by username
    public User getUserByUsername(String userName) {
        return userDAO.getUserByUsername(userName);
    }

}
