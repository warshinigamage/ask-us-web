package com.iit.askus.controller;

import com.iit.askus.service.QuestionService;
import com.iit.askus.service.StudentService;
import com.iit.askus.view.SearchForm;
import com.iit.askus.view.StudentRegisterForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

@Controller
@SessionAttributes("studentForm")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/register/student", method = RequestMethod.GET)
    public ModelAndView createStudentRegistrationForm(ModelMap model) {
        StudentRegisterForm studentRegistrationForm = studentService.createStudentRegistrationForm();
        model.addAttribute("studentForm", studentRegistrationForm);
        return new ModelAndView("register-student", "model", model);
    }

    @RequestMapping(value = "/register/student", method = RequestMethod.POST)
    public ModelAndView submitStudentRegistrationForm(@ModelAttribute("studentForm") StudentRegisterForm studentRegistrationForm,
                                    BindingResult result, HttpServletRequest request) {
        String validationMessage = validateStudentRegistry(studentRegistrationForm);
        Map model = new HashMap();

        if(validationMessage != null){
            model.put("errorNotice", validationMessage);
            return new ModelAndView("register-student", "model", model);
        } else {
            try {
                studentService.saveStudent(studentRegistrationForm);
            } catch (ParseException e) {
                model.put("errorNotice", "Invalid Data Format. Please Select again from the Calendar");
                return new ModelAndView("register-student", "model", model);
            }

            return new ModelAndView("redirect:/login?ureg=student", "model", model);
        }
    }

    public String validateStudentRegistry(StudentRegisterForm studentRegisterForm){
        if(studentRegisterForm.getUserName().trim().isEmpty()) {
            return "Username is required!";
        } else if(studentRegisterForm.getPassword().trim().isEmpty()) {
            return "Password is required";
        } else if(studentRegisterForm.getFirstName().trim().isEmpty()){
            return "First Name is required!";
        } else if(studentRegisterForm.getLastName().trim().isEmpty()) {
            return "Last Name is required!";
        } else if(studentRegisterForm.getBirthDate().trim().isEmpty()) {
            return "Birthdate is required!";
        } else if (studentRegisterForm.getEmailAddress().trim().isEmpty()) {
            return "Email address is required!";
        } else if (studentRegisterForm.getNicNo().trim().isEmpty()) {
            return "NIC number is required!";
        } else if (studentRegisterForm.getSchool().trim().isEmpty()) {
            return "School name is required!";
        } else return null;
    }

}
