package com.iit.askus.controller;

import com.iit.askus.model.Answer;
import com.iit.askus.model.Lesson;
import com.iit.askus.model.Question;
import com.iit.askus.model.User;
import com.iit.askus.service.AnswerService;
import com.iit.askus.service.LessonService;
import com.iit.askus.service.QuestionService;
import com.iit.askus.view.AnswerForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
public class QuestionController {
    @Autowired
    private LessonService lessonService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private AnswerService answerService;


    @RequestMapping(value = "view/question/{questionId}", method = RequestMethod.GET)
    public ModelAndView viewQuestion(@PathVariable String questionId, ModelMap model, HttpServletRequest request) {
        Question question = questionService.getQuestionByQuestionId(Long.valueOf(questionId));
        List<Answer> answerList = answerService.getAnswersByQuestionId(Long.valueOf(questionId));

        model.put("question", question);

        //create answer submission form
        User user = (User) request.getSession().getAttribute("user");
        AnswerForm answerForm = answerService.createAnswerForm(String.valueOf(user.getUserId()));
        model.put("answerForm", answerForm);

        if(answerList.isEmpty()) {
            model.put("emptyListMsg", "No answers have been submitted for the question!");
            return new ModelAndView("view-question", "model", model);
        } else {
            //sort the list in order to get the highest rating on top
            Collections.sort(answerList);

            List<Map<String, Object>> answersMapList = new ArrayList<Map<String, Object>>();
            for(int i=0; i<answerList.size(); i++) {
                Map<String, Object> answersMap= new HashMap<String, Object>();
                answersMap.put("answerId", answerList.get(i).getAnswerId());
                answersMap.put("answer", answerList.get(i).getAnswer());
                answersMap.put("answerTutor", answerList.get(i).getTutor());
                if(answerList.get(i).getRateCount() == 0) {
                    answersMap.put("answerRating", "-");
                } else {
                    answersMap.put("answerRating", String.format("%.2f", (answerList.get(i).getRateSum() * 1.0) / answerList.get(i).getRateCount()));
                }
                answersMapList.add(answersMap);
            }

            model.put("answerList", answersMapList);
            return new ModelAndView("view-question", "model", model);
        }
    }

    @RequestMapping(value = "view/question/{questionId}", method = RequestMethod.POST)
    public ModelAndView submitAnswerForm(@ModelAttribute("questionForm") AnswerForm answerForm,
                                                      BindingResult result) {
        String validationMessage = validateAnswerSubmit(answerForm);
        Map model = new HashMap();

        if(validationMessage != null){
            return new ModelAndView("redirect:/home?asub=false", "model", model);
        } else {
            answerService.saveAnswer(answerForm);
            return new ModelAndView("redirect:/home?asub=true", "model", model);
        }
    }

    public String validateAnswerSubmit(AnswerForm answerForm){
        if(answerForm.getAnswer().trim().isEmpty()) {
            return "You haven't typed a answer!";
        } else return null;
    }

}