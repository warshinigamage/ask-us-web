package com.iit.askus.controller;

import com.iit.askus.model.*;
import com.iit.askus.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@Controller
public class HomeController {

    @Autowired
    private UserService userService;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private LessonService lessonService;

    @Autowired
    private TutorService tutorService;


    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView loadHomePage(HttpServletRequest request, HttpServletResponse response, ModelMap model)  throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getUserByUsername(auth.getName());
        request.getSession().setAttribute("user", user);

        List<Subject> subjectList = new ArrayList<Subject>();

        model = formSubmissionMsg(request, model, "qsub", "Your question has been submitted",
                "Error occurred while submitting question.");
        model = formSubmissionMsg(request, model, "asub", "Your answer has been submitted",
                "Error occurred while submitting answer.");

        if(user.getUserGuardGroup().getUserGroupName().equals("ROLE_ADMIN_GROUP")){
            List<Tutor> tutorList = tutorService.getPendingTutors();
            if(tutorList.isEmpty()) {
                model.addAttribute("pendingTutorListEmptyMsg", "There are no tutors to be approved!");
            } else {
                model.addAttribute("pendingTutorList", tutorList);
            }
        } else {
            if(user.getTutor() != null){
                subjectList.add(user.getTutor().getQualifiedSubject());
            } else {
                subjectList = subjectService.getAllSubjects();
            }
            List<Map<String, Object>> subjectsWithLessons = new ArrayList<Map<String, Object>>();

            for(int i=0; i<subjectList.size(); i++) {
                Map<String, Object> subjectWithLessonsMap= new HashMap<String, Object>();
                subjectWithLessonsMap.put("subject", subjectList.get(i));
                subjectWithLessonsMap.put("lessons", lessonService.getLessonsBySubjectId(subjectList.get(i).getSubjectId()));
                subjectsWithLessons.add(subjectWithLessonsMap);
            }

            model.addAttribute("subjectsWithLessons", subjectsWithLessons);
        }


        return new ModelAndView("home", "model", model);
    }

    private ModelMap formSubmissionMsg(HttpServletRequest request, ModelMap model, String queryParam,
                                       String successMsg, String errorMsg) {
        String isQuestionSubmission = request.getParameter(queryParam);
        if(isQuestionSubmission != null) {
            if(isQuestionSubmission.equals("true")) {
                model.put("successNotice", successMsg);
            } else if(isQuestionSubmission.equals("false")) {
                model.put("errorNotice", errorMsg);
            }
        }

        return model;
    }



}
