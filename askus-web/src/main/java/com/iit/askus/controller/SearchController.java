package com.iit.askus.controller;

import com.iit.askus.model.Question;
import com.iit.askus.service.QuestionService;
import com.iit.askus.view.SearchForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@SessionAttributes("searchForm")
public class SearchController {

    @Autowired
    QuestionService questionService;

    @RequestMapping(value = "/search/subjects", method = RequestMethod.POST)
    public @ResponseBody List<Question> submitSearchForm(@ModelAttribute("searchForm") SearchForm searchForm) {
        List<Question> questionList =
                questionService.searchQuestion(searchForm.getSearchQuery(), searchForm.getSubject());

        return questionList;

    }
}
