package com.iit.askus.controller;

import com.iit.askus.service.LessonService;
import com.iit.askus.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SubjectController {
    @Autowired
    private SubjectService subjectService;

    @Autowired
    private LessonService lessonService;

    @RequestMapping(value = "view/subject/{subjectId}", method = RequestMethod.GET)
    public ModelAndView viewSubject(@PathVariable String subjectId, ModelMap model) {
        model.put("subject", subjectService.getSubjectBySubjectId(Long.valueOf(subjectId)));
        model.put("lessonList", lessonService.getLessonsBySubjectId(Long.valueOf(subjectId)));
        return new ModelAndView("view-subject", "model", model);
    }


}