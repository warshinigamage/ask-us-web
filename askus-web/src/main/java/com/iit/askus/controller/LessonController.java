package com.iit.askus.controller;

import com.iit.askus.model.Lesson;
import com.iit.askus.model.Question;
import com.iit.askus.model.User;
import com.iit.askus.service.LessonService;
import com.iit.askus.service.QuestionService;
import com.iit.askus.view.QuestionForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.*;

@Controller
public class LessonController {
    @Autowired
    private LessonService lessonService;

    @Autowired
    private QuestionService questionService;


    @RequestMapping(value = "view/lesson/{lessonId}", method = RequestMethod.GET)
    public ModelAndView viewLesson(@PathVariable String lessonId, ModelMap model, HttpServletRequest request) {
        Lesson lesson = lessonService.getLessonByLessonId(Long.valueOf(lessonId));
        List<Question> questionList = questionService.getQuestionsByLessonId(Long.valueOf(lessonId));

        model.put("lesson", lesson);

        //create question submission form
        User user = (User) request.getSession().getAttribute("user");
        QuestionForm questionForm = questionService.createQuestionForm(String.valueOf(user.getUserId()));
        model.put("questionForm", questionForm);

        if(questionList.isEmpty()) {
            model.put("emptyListMsg", "No questions have been submitted for the lesson!");
            return new ModelAndView("view-lesson", "model", model);
        } else {
            //sort the list in order to get the highest rating on top
            Collections.sort(questionList);

            List<Map<String, Object>> questionsMapList = new ArrayList<Map<String, Object>>();
            for(int i=0; i<questionList.size(); i++) {
                Map<String, Object> questionsMap= new HashMap<String, Object>();
                questionsMap.put("questionId", questionList.get(i).getQuestionId());
                questionsMap.put("question", questionList.get(i).getQuestion());
                questionsMap.put("questionStudent", questionList.get(i).getStudent());
                if(questionList.get(i).getRateCount() == 0) {
                    questionsMap.put("questionRating", "-");
                } else {
                    questionsMap.put("questionRating", String.format("%.2f", (questionList.get(i).getRateSum() * 1.0) / questionList.get(i).getRateCount()));
                }
                questionsMapList.add(questionsMap);
            }

            model.put("questionList", questionsMapList);
            return new ModelAndView("view-lesson", "model", model);
        }
    }


    @RequestMapping(value = "view/lesson/{lessonId}", method = RequestMethod.POST)
    public ModelAndView submitQuestionForm(@ModelAttribute("questionForm") QuestionForm questionForm,
                                                      BindingResult result) {
        String validationMessage = validateQuestionSubmit(questionForm);
        Map model = new HashMap();

        if(validationMessage != null){
            return new ModelAndView("redirect:/home?qsub=false", "model", model);
        } else {
            questionService.saveQuestion(questionForm);
            return new ModelAndView("redirect:/home?qsub=true", "model", model);
        }
    }

    public String validateQuestionSubmit(QuestionForm questionForm){
        if(questionForm.getQuestion().trim().isEmpty()) {
            return "You haven't typed a question!";
        } else return null;
    }

}