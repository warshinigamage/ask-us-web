package com.iit.askus.controller;

import com.iit.askus.service.QuestionService;
import com.iit.askus.view.SearchForm;
import com.iit.askus.view.StudentRegisterForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@SessionAttributes("loginForm")
public class AuthenticationController {

    @Autowired
    QuestionService questionService;

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public ModelAndView welcomeUser(Model model, Principal principal) {
        String name = principal.getName();
        model.addAttribute("userName", name);
        return new ModelAndView("welcome","model",model);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView defaultPage(ModelMap model, HttpServletRequest request) {
        return authenticateLogin(model, request);
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(ModelMap model, HttpServletRequest request) {
        model = formSubmissionMsg(request, model, "ureg");
        return authenticateLogin(model, request);
    }

    @RequestMapping(value = "/loginfailed", method = RequestMethod.GET)
    public String loginError(Model model, HttpServletRequest request) {
        SearchForm searchForm = questionService.createSearchForm();
        request.getSession().setAttribute("searchForm", searchForm);
        model.addAttribute("searchForm", searchForm);

        model.addAttribute("error", "true");
        return "login";

    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(Model model) {
        return "login";

    }

    private ModelAndView authenticateLogin(ModelMap model, HttpServletRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        SearchForm searchForm = questionService.createSearchForm();
        request.getSession().setAttribute("searchForm", searchForm);
        model.addAttribute("searchForm", searchForm);

        if (!(auth instanceof AnonymousAuthenticationToken)) {
            return new ModelAndView("redirect:/home", "model", model);
        } else {

            return new ModelAndView("login", "model", model);
        }
    }

    private ModelMap formSubmissionMsg(HttpServletRequest request, ModelMap model, String queryParam) {
        String userRegSubmission = request.getParameter(queryParam);
        if(userRegSubmission != null) {
            if(userRegSubmission.equals("student")) {
                model.put("successNotice", "You Have Successfully Registered! Login with your username and password.");
            } else if(userRegSubmission.equals("tutor")) {
                model.put("successNotice", "You Have Successfully Registered! Please await for an email with your Skype interview time slot");
            }
        }

        return model;
    }



}
