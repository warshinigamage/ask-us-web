package com.iit.askus.controller;

import com.iit.askus.service.QuestionService;
import com.iit.askus.service.StudentService;
import com.iit.askus.service.TutorService;
import com.iit.askus.view.SearchForm;
import com.iit.askus.view.TutorRegisterForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

@Controller
@SessionAttributes("tutorForm")
public class TutorController {

    @Autowired
    private TutorService tutorService;

    @RequestMapping(value = "/register/tutor", method = RequestMethod.GET)
    public ModelAndView createTutorRegistrationForm(ModelMap model) {
        TutorRegisterForm tutorRegisterForm = tutorService.createTutorRegistrationForm();
        model.addAttribute("tutorForm", tutorRegisterForm);
        return new ModelAndView("register-tutor", "model", model);
    }

    @RequestMapping(value = "/register/tutor", method = RequestMethod.POST)
    public ModelAndView submitTutorRegistrationForm(@ModelAttribute("tutorForm") TutorRegisterForm tutorRegisterForm,
                                    BindingResult result, HttpServletRequest request) {
        String validationMessage = validateTutorRegistry(tutorRegisterForm);
        Map model = new HashMap();

        if(validationMessage != null){
            model.put("errorNotice", validationMessage);
            return new ModelAndView("register-tutor", "model", model);
        } else {
            try {
                tutorService.saveTutor(tutorRegisterForm);
            } catch (ParseException e) {
                model.put("errorNotice", "Invalid Data Format. Please Select again from the Calendar");
                return new ModelAndView("register-tutor", "model", model);
            }

            return new ModelAndView("redirect:/login?ureg=tutor", "model", model);
        }
    }

    public String validateTutorRegistry(TutorRegisterForm tutorRegisterForm){
        if(tutorRegisterForm.getUserName().trim().isEmpty()) {
            return "Username is required!";
        } else if(tutorRegisterForm.getPassword().trim().isEmpty()) {
            return "Password is required";
        } else if(tutorRegisterForm.getFirstName().trim().isEmpty()){
            return "First Name is required!";
        } else if(tutorRegisterForm.getLastName().trim().isEmpty()) {
            return "Last Name is required!";
        } else if(tutorRegisterForm.getBirthDate().trim().isEmpty()) {
            return "Birthdate is required!";
        } else if (tutorRegisterForm.getEmailAddress().trim().isEmpty()) {
            return "Email address is required!";
        } else if (tutorRegisterForm.getNicNo().trim().isEmpty()) {
            return "NIC number is required!";
        } else if (tutorRegisterForm.getSkypeId().trim().isEmpty()) {
            return "Skype Id is required!";
        } else if (tutorRegisterForm.getEducationalQualifications().trim().isEmpty()) {
            return "Educational qualifications are required!";
        } else if (tutorRegisterForm.getProfessionalQualifications().trim().isEmpty()) {
            return "Professional qualifications are required!";
        } else return null;
    }


    @RequestMapping(value = "approve/tutor/{tutorId}", method = RequestMethod.GET)
    public ModelAndView viewLesson(@PathVariable String tutorId, ModelMap model) {
        tutorService.approveTutor(Integer.parseInt(tutorId));
        return new ModelAndView("redirect:/home", "model", model);
    }

}
