function loadLesson(lessonId) {
    $.ajax({
        type: "GET",
        url: "view/lesson/" + lessonId,
        success: function (data) {
            $( '.tab-pane.active' ).html(data);
        }
    });
}

/*function loadLessonPost(lessonId) {
    var str = $("#questionForm").serialize();
    $.ajax({
        type: "POST",
        data:str,
        url: "view/lesson/" + lessonId,
        success: function (data) {
            $( '.tab-pane.active' ).html(data);
        }
    });
}*/

function loadSubject(subjectId) {
    $.ajax({
        type: "GET",
        url: "view/subject/" + subjectId,
        success: function (data) {
            $( '.tab-pane.active' ).html(data);
        }
    });
}

function loadQuestion(questionId) {
    $.ajax({
        type: "GET",
        url: "view/question/" + questionId,
        success: function (data) {
            $( '.tab-pane.active' ).html(data);
        }
    });
}