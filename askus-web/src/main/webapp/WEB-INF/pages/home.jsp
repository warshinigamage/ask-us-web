<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>

<head>
    <jsp:include page="decorators/resources.jsp"/>
</head>

<body>

<jsp:include page="decorators/navbar.jsp"/>

<div style="background-image: url(<c:url value='/img/bg-mid.png'/>); background-repeat: repeat-y;" class="container"
     id="container-big">
<div class="row">
<div class="span22 offset1" style="padding-top: 50px;">

<div class="row" style="padding-bottom: 10px;">
    <div class="span22">
        <c:if test="${not empty model.successNotice}">
            <div class="alert alert-success" style="text-align: center; margin-bottom: -5px;">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <b>${model.successNotice}</b>
            </div>
        </c:if>
        <c:if test="${not empty model.errorNotice}">
            <div class="alert alert-error" style="text-align: center; margin-bottom: -5px;">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <b>${model.errorNotice}</b>
            </div>
        </c:if>
    </div>
</div>


<div class="row" style="padding-top: 10px;">
    <div class="span20 offset1">
        <div id="liquid-round">
            <div class="top"><span></span></div>
            <div class="center-content">
                <div class="row" style="padding-bottom: 20px; padding-left: 15px;">
                    <div class="span18">
                        <sec:authorize ifAnyGranted="ROLE_TUTOR_GROUP, ROLE_STUDENT_GROUP">
                            <div class="tab-border">
                                <ul id="subjectsTab" class="nav nav-tabs" style="border-top: none;">
                                    <c:forEach begin="0" end="0" items="${model.subjectsWithLessons}" var="current">
                                        <li class="active"><a href="#${current.subject.subjectName}" data-toggle="tab">${current.subject.subjectName}</a></li>
                                    </c:forEach>
                                    <c:forEach begin="1" items="${model.subjectsWithLessons}" var="current">
                                        <li><a href="#${current.subject.subjectName}" data-toggle="tab">${current.subject.subjectName}</a></li>
                                    </c:forEach>
                                </ul>
                                <%--Tab Content--%>
                                <div id="subjectsTabContent" class="tab-content" style="padding: 10px;">
                                    <c:forEach begin="0" end="0" items="${model.subjectsWithLessons}" var="current">
                                        <div class="tab-pane fade active in" id="${current.subject.subjectName}">
                                            <div class="row">
                                                <div class="span17" style="padding-left: 10px;">
                                                    <h2 class="title-color">${current.subject.subjectName} Lessons</h2>
                                                    <ul>
                                                        <c:forEach begin="0" items="${current.lessons}" var="lesson">
                                                            <li style="line-height: 22px;">
                                                                <a href="#" onclick="loadLesson('${lesson.lessonId}')">${lesson.lessonName}</a>
                                                            </li>
                                                        </c:forEach>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>

                                    <c:forEach begin="1" items="${model.subjectsWithLessons}" var="current">
                                        <div class="tab-pane fade in" id="${current.subject.subjectName}">
                                            <div class="row">
                                                <div class="span17" style="padding-left: 10px;">
                                                    <h2 class="title-color">${current.subject.subjectName} Lessons </h2>
                                                    <ul>
                                                        <c:forEach begin="0" items="${current.lessons}" var="lesson">
                                                            <li style="line-height: 22px;"><a href="#" onclick="loadLesson('${lesson.lessonId}')">${lesson.lessonName}</a></li>
                                                        </c:forEach>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>

                                </div>

                            </div>
                            </sec:authorize>

                        <sec:authorize ifAnyGranted="ROLE_ADMIN_GROUP">
                            <div style="padding-top: 20px"></div>
                        <div class="tab-border">
                            <ul id="usersTab" class="nav nav-tabs" style="border-top: none;">
                                    <li class="active"><a href="#pendingTutors" data-toggle="tab">Tutors Pending for Approval</a></li>
                                    <li><a href="#allUsers" data-toggle="tab">All Users</a></li>
                            </ul>

                            <div id="usersTabContent" class="tab-content" style="padding: 10px;">
                                    <div class="tab-pane fade active in" id="pendingTutors">
                                        <div class="row">
                                            <div class="span17" style="padding-left: 10px;">
                                                <h2 class="title-color" style="padding-bottom: 10px;">Tutors Pending for Approval</h2>
                                                <c:if test="${not empty model.pendingTutorList}">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>Full Name</th>
                                                        <th>Qualifications</th>
                                                        <th>Applying Subject</th>
                                                        <th>Skype Id</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <c:forEach begin="0" items="${model.pendingTutorList}" var="current">
                                                        <tr>
                                                            <td>${current.user.firstName} ${current.user.lastName}</td>
                                                            <td>
                                                                <ul>
                                                                    <li><i>Education</i> : ${current.educationalQualifications}</li>
                                                                    <li><i>Professional</i> : ${current.professionalQualifications}</li>
                                                                </ul>
                                                            </td>
                                                            <td>${current.qualifiedSubject.subjectName}</td>
                                                            <td>${current.skypeId}</td>
                                                            <td><a href="approve/tutor/${current.userId}">Approve</a> </td>
                                                        </tr>
                                                    </c:forEach>
                                                    </tbody>
                                                </table>
                                                </c:if>

                                                <c:if test="${not empty model.pendingTutorListEmptyMsg}">
                                                  <p style="padding-top: 10px;">${model.pendingTutorListEmptyMsg}</p>
                                                </c:if>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade in" id="allUsers">
                                        <div class="row">
                                            <div class="span17" style="padding-left: 10px;">
                                                <h2 class="title-color">All Users</h2>

                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>Full Name</th>
                                                        <th>Type</th>
                                                        <th>Username</th>
                                                        <th>Email Address</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>n/a</td>
                                                        <td>n/a</td>
                                                        <td>n/a</td>
                                                        <td>n/a</td>
                                                        <td>n/a</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                            </div>
                         </div>
                        </sec:authorize>
                        <div style="padding-bottom: 30px;"></div>
                    </div>
                </div>


            </div>
            <div class="bottom"><span></span></div>
        </div>
    </div>
    <div class="span5"><span></span></div>
</div>

</div>
</div>
<div style="padding-top: 80px;"></div>
</div>

<jsp:include page="decorators/footer.jsp"/>


</body>

</html>