<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="row">
    <div class="span17" style="padding-left: 10px;">
        <h3 class="title-color" style="padding-bottom: 10px;">
            ${model.question.question}
        </h3>
    </div>
</div>
<div class="row">
    <div class="span17" style="padding-left: 10px;">
        <c:if test="${not empty model.emptyListMsg}">
            <p>${model.emptyListMsg}</p>
        </c:if>

        <c:if test="${empty model.emptyListMsg}">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Answer</th>
                    <th>Tutor</th>
                    <th>Rating</th>
                    <th>Enter Rate</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach begin="0" items="${model.answerList}" var="current">
                    <tr>
                        <td>${current.answer}</td>
                        <td>${current.answerTutor.user.firstName} ${current.answerTutor.user.lastName}</td>
                        <td>${current.answerRating}</td>
                        <td>n/a</td>
                    </tr>
                </c:forEach>

                </tbody>
            </table>
        </c:if>

        <br/>

        <sec:authorize ifAnyGranted="ROLE_TUTOR_GROUP">
            <div class="well">
                <form:form id="answerForm" method="POST" commandName="answerForm" modelAttribute="answerForm"
                           cssStyle="margin-bottom: 0px">
                    <fieldset>
                        <div class="row">
                            <div class="span16">
                                <div class="clearfix">
                                    <label><b> Post Your Answer</b></label>

                                    <div class="input">
                                        <form:textarea path="answer" class="input-block-level" id="answer"
                                                       type="textarea"></form:textarea>

                                        <form:hidden path="questionId" id="questionId" value="${model.question.questionId}"/>
                                        <form:hidden path="tutorId" id="tutorId"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="element-top-padding"><span></span></div>
                        <button type="submit" class="btn"
                                style="padding-left: 10px; padding-right: 10px;" value="Submit">
                            Submit
                        </button>

                    </fieldset>

                </form:form>
            </div>
        </sec:authorize>

        <button class="btn btn-primary" onclick="loadLesson('${model.question.lesson.lessonId}')"
                style="padding-left: 10px; padding-right: 10px;">
            Go Back
        </button>

    </div>
</div>

