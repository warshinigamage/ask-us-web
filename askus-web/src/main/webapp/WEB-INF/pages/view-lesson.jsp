<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="row">
    <div class="span17" style="padding-left: 10px;">
        <h2 class="title-color" style="padding-bottom: 10px;">
            ${model.lesson.lessonName}
        </h2>
    </div>
</div>
<div class="row">
    <div class="span17" style="padding-left: 10px;">
        <c:if test="${not empty model.emptyListMsg}">
            <p>${model.emptyListMsg}</p>
        </c:if>

        <c:if test="${empty model.emptyListMsg}">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Question</th>
                    <th>Student</th>
                    <th>Rating</th>
                    <th>Enter Rate</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach begin="0" items="${model.questionList}" var="current">
                    <tr>
                        <td><a href="#" onclick="loadQuestion('${current.questionId}')">${current.question}</a></td>
                        <td>${current.questionStudent.user.firstName} ${current.questionStudent.user.lastName}</td>
                        <td>${current.questionRating}</td>
                        <td>n/a</td>
                    </tr>
                </c:forEach>

                </tbody>
            </table>
        </c:if>

        <br/>
        <sec:authorize ifAnyGranted="ROLE_STUDENT_GROUP">
            <div class="well">
                <form:form id="questionForm" method="POST" commandName="questionForm" modelAttribute="questionForm"
                           cssStyle="margin-bottom: 0px">
                    <fieldset>
                        <div class="row">
                            <div class="span16">
                                <div class="clearfix">
                                    <label><b> Post Your Question</b></label>

                                    <div class="input">
                                        <form:textarea path="question" class="input-block-level" id="question"
                                                       type="textarea"></form:textarea>

                                        <form:hidden path="lessonId" id="lessonId" value="${model.lesson.lessonId}"/>
                                        <form:hidden path="studentId" id="studentId"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="element-top-padding"><span></span></div>
                        <button type="submit" class="btn"
                                style="padding-left: 10px; padding-right: 10px;" value="Submit">
                            Submit
                        </button>

                    </fieldset>

                </form:form>
            </div>
        </sec:authorize>

        <button class="btn btn-primary" onclick="loadSubject('${model.lesson.subject.subjectId}')"
                style="padding-left: 10px; padding-right: 10px;">
            Go Back
        </button>
        <div class="element-top-padding"><span></span></div>
    </div>
</div>

