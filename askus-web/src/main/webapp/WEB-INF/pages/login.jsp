<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>

<head>
    <jsp:include page="decorators/resources.jsp"/>
</head>

<body onload='document.f.j_username.focus();'>

<jsp:include page="decorators/navbar.jsp"/>

<div style="background-image: url(<c:url value='/img/bg-mid.png' />); background-repeat: repeat-y;" class="container"
     id="container-big">
    <div class="row">
        <div class="span22 offset1">
            <jsp:include page="decorators/banner.jsp"/>

            <div class="row">
                <div class="span22">

                    <div class="well" style="background-color: #e8e8e8">

                        <form:form id="searchFormId" method="POST" commandName="searchForm" modelAttribute="searchForm" action="search/subjects" cssStyle="margin-bottom: -5px;">
                            <fieldset>
                                <div class="row">
                                    <div class="span10">
                                        <div class="clearfix">
                                            <div class="input">
                                                <form:input path="searchQuery" class="input-block-level" id="searchQuery" placeholder="Search"
                                                            type="text"></form:input>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="span8">
                                        <div class="clearfix">
                                            <div class="input">
                                                <form:select path="subject" id="subject" class="input-block-level" multiple="false">
                                                    <c:forEach begin="0" items="${searchForm.subjectList}" var="current" varStatus="status">
                                                        <form:option value="${status.index}">${current}</form:option>
                                                    </c:forEach>
                                                </form:select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="span3">
                                        <div class="clearfix">
                                            <button type="submit" class="btn btn-primary input-block-level"
                                                    style="padding-left: 10px; padding-right: 10px;" value="Search">
                                                Search
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                    </fieldset>
                        </form:form>

                    </div>
                </div>
            </div>

            <c:if test="${not empty error}">
                <div class="row">
                    <div class="span22">
                        <div class="alert alert-error" style="display: ${model.displayErrorNotice};
                                text-align: center; padding-bottom: 20px;">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <b>Your login attempt was not successful, try again. Cause :
                                    ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}</b>
                        </div>
                    </div>
                </div>
            </c:if>


            <div class="row">
                <div class="span22">
                    <c:if test="${not empty model.successNotice}">
                        <div class="alert alert-success" style="text-align: center;  padding-bottom: 20px;">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <b>${model.successNotice}</b>
                        </div>
                    </c:if>
                </div>
            </div>


            <div class="row-fluid">

                <div class="span6">
                    <p>AskUs is an amazing online tutoring system for GCE Adavanced Level Bio students.
                        We cover all the three subjects, Chemistry, Physics and Bio Science.
                        <br/><br/> You can join us as;
                    </p>
                    <h5 class="title-color">A STUDENT</h5>
                    <p>Ask your questions and get best answers from the subject experts. You may also ask for more
                        clarifications from the tutors. Learn from others' questions and answers.
                        Rate them as how useful those to you.</p>
                    <button onClick="location.href='register/student'" class="btn btn-primary"
                            style="padding-left: 10px; padding-right: 10px;">
                        Register as a Student
                    </button>


                    <h5 class="title-color" style="padding-top: 10px;">A TUTOR</h5>
                    <p>Share your knowledege with the students and get goog ratings for your answers.</p>
                    <button onClick="location.href='register/tutor'" class="btn btn-primary"
                            style="padding-left: 10px; padding-right: 10px;">
                        Apply as a Tutor
                    </button>

                </div>


                <div class="span6">
                    <div id="liquid-round">
                        <div class="top"><span></span></div>
                        <div class="center-content">
                            <div class="row">
                                <div class="span10 offset1">
                                    <div style="position: relative; z-index: 10">
                                        <h2 class="title-color" style="padding-bottom: 10px; padding-top: 10px;">
                                            Sign In
                                        </h2>

                                        <form name='f' action="<c:url value='/j_spring_security_check' />" method='POST'>
                                            <fieldset>
                                                <div class="clearfix">
                                                    <label><b>Username</b></label>

                                                    <div class="input">
                                                        <input class="xlarge" name="j_username" size="25"
                                                               type="text" />
                                                    </div>
                                                </div>

                                                <div class="clearfix">
                                                    <label> <b>Password</b></label>

                                                    <div class="input">
                                                        <input class="xlarge" name="j_password" size="25"
                                                               type="password"/>
                                                    </div>
                                                </div>

                                                <div class="element-top-padding"><span></span></div>
                                                <button type="submit" class="btn btn-primary"
                                                        style="padding-left: 10px; padding-right: 10px;">
                                                    Sign In
                                                </button>


                                            </fieldset>
                                        </form>

                                        <div class="link-wo-underline" style="padding-bottom: 20px; padding-top: 10px;">
                                            <a href="forgot-password">Forgot your password?</a>
                                        </div>

                                        <div class="element-top-padding"><span></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bottom"><span></span></div>
                    </div>
                </div>
                <div class="span5"><span></span></div>
            </div>

        </div>
    </div>
    <div style="padding-top: 80px;"></div>
</div>


<jsp:include page="decorators/footer.jsp"/>


</body>

</html>

