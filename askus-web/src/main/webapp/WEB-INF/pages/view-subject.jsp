<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row">
    <div class="span17" style="padding-left: 10px;">
        <h2 class="title-color">${model.subject.subjectName} Lessons</h2>
        <ul>
            <c:forEach begin="0" items="${model.lessonList}" var="lesson">
                <li style="line-height: 22px;"><a href="#" onclick="loadLesson('${lesson.lessonId}')">${lesson.lessonName}</a></li>
            </c:forEach>
        </ul>
    </div>
</div>

