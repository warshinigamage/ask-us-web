<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>

<head>
    <jsp:include page="decorators/resources.jsp"/>
    <script src="<c:url value='/js/jquery/jquery-ui-1.8.16.custom.min.js' />" type="text/javascript"></script>
    <link rel="stylesheet" href="<c:url value='/css/jquery/jquery-ui-1.8.16.custom.css' />"/>
</head>

<body>

<script>
    $(document).ready(function () {
        $("#birthDate").datepicker({
        });
    });
</script>

<jsp:include page="decorators/navbar.jsp"/>

<div style="background-image: url(<c:url value='/img/bg-mid.png'/>); background-repeat: repeat-y;" class="container"
     id="container-big">
    <div class="row">
        <div class="span22 offset1" style="padding-top: 50px;">

            <div class="row">
                <div class="span22">
                    <c:if test="${not empty model.successNotice}">
                        <div class="alert alert-success" style="text-align: center; margin-bottom: -5px;">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <b>${model.successNotice}</b>
                        </div>
                    </c:if>
                    <c:if test="${not empty model.errorNotice}">
                        <div class="alert alert-error" style="text-align: center; margin-bottom: -5px;">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <b>${model.errorNotice}</b>
                        </div>
                    </c:if>
                </div>
            </div>

            <div class="row" style="padding-top: 10px;">
                <div class="span20 offset1">
                    <div id="liquid-round">
                        <div class="top"><span></span></div>
                        <div class="center-content">
                            <div class="row">
                                <div class="span10 offset1">
                                    <div style="position: relative; z-index: 10">
                                        <h2 class="title-color">
                                            Apply as a Tutor
                                        </h2>
                                        <div class="row" style="padding-bottom: 5px;">
                                            <div class="span19">
                                                <div class="clearfix">
                                                    <span style="color: #a9a9a9;">Note: All fields are required.</span>
                                                </div>
                                            </div>
                                        </div>

                                        <form:form id="registerTutorForm" method="POST" commandName="tutorForm"
                                                   modelAttribute="tutorForm">
                                            <fieldset>
                                                <div class="row">
                                                    <div class="span19">
                                                        <h5 class="title-color" style="line-height: 20px;">
                                                            Personal Information
                                                        </h5>
                                                        <hr style="margin: 0px 0px 10px 0px; width: 640px; border-top-color: #0088cc;"/>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="span9">
                                                        <div class="clearfix">
                                                            <label><b> User Name</b></label>

                                                            <div class="input">
                                                                <form:input path="userName" class="input-xlarge" id="userName"
                                                                            type="text"></form:input>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="span9">
                                                        <div class="clearfix">
                                                            <label><b> Password</b></label>

                                                            <div class="input">
                                                                <form:input path="password" class="input-xlarge" id="password"
                                                                            type="text"></form:input>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="span9">
                                                        <div class="clearfix">
                                                            <label><b> First Name</b></label>

                                                            <div class="input">
                                                                <form:input path="firstName" class="input-xlarge" id="firstName"
                                                                            type="text"></form:input>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="span9">
                                                        <div class="clearfix">
                                                            <label><b> Last Name</b></label>

                                                            <div class="input">
                                                                <form:input path="lastName" class="input-xlarge" id="lastName"
                                                                            type="text"></form:input>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="span9">
                                                        <div class="clearfix">
                                                            <label><b> Birth Date</b></label>

                                                            <div class="input">
                                                                <form:input path="birthDate" class="input-xlarge" id="birthDate" type="text"></form:input>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="span9">
                                                        <div class="clearfix">
                                                            <label><b> Gender</b></label>

                                                            <div class="input">
                                                                <form:select path="gender" id="gender" style="width: 280px;">
                                                                    <c:forEach begin="0" items="${tutorForm.genderList}" var="current">
                                                                        <form:option value="${current}"/>
                                                                    </c:forEach>
                                                                </form:select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="span9">
                                                        <div class="clearfix">
                                                            <label><b> E-Mail Address</b></label>

                                                            <div class="input">
                                                                <form:input path="emailAddress" class="input-xlarge" id="emailAddress"
                                                                            type="text"></form:input>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="span9">
                                                        <div class="clearfix">
                                                            <label><b> NIC Number</b> </label>

                                                            <div class="input">
                                                                <form:input path="nicNo" class="input-xlarge" id="nicNo"
                                                                            type="text"></form:input>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="span9">
                                                        <div class="clearfix">
                                                            <label><b> Skype ID</b> </label>

                                                                <div class="input">
                                                                    <form:input path="skypeId" class="input-xlarge" id="skypeId"
                                                                                type="text"></form:input>
                                                                </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="span19">
                                                        <h5 class="title-color" style="line-height: 20px;">
                                                            Professional Information
                                                        </h5>
                                                        <hr style="margin: 0px 0px 10px 0px; width: 640px; border-top-color: #0088cc;"/>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="span9">
                                                        <div class="clearfix">
                                                            <label><b> Qualified Subject</b></label>

                                                            <div class="input">
                                                                <form:select path="qualifiedSubject" id="qualifiedSubject" style="width: 280px;">
                                                                    <c:forEach begin="0" items="${tutorForm.qualifiedSubjectList}" var="current">
                                                                        <form:option value="${current}"/>
                                                                    </c:forEach>
                                                                </form:select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="span9">
                                                        <div class="clearfix">
                                                            <label><b> Educational Qualifications</b></label>

                                                            <div class="input">
                                                                <form:input path="educationalQualifications" class="input-xlarge" id="educationalQualifications"
                                                                            type="textarea"></form:input>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="span9">
                                                        <div class="clearfix">
                                                            <label><b> Professional Qualifications</b></label>

                                                            <div class="input">
                                                                <form:input path="professionalQualifications" class="input-xlarge" id="professionalQualifications"
                                                                            type="textarea"></form:input>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="element-top-padding"><span></span></div>
                                                <button type="submit" class="btn btn-primary"
                                                        style="padding-left: 10px; padding-right: 10px;" value="Submit">
                                                    Submit
                                                </button>

                                            </fieldset>
                                        </form:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bottom"><span></span></div>
                    </div>
                </div>
                <div class="span5"><span></span></div>
            </div>

        </div>
    </div>
    <div style="padding-top: 80px;"></div>
</div>

<jsp:include page="decorators/footer.jsp"/>


</body>

</html>