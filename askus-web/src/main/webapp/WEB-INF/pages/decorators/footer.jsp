<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="footer-area" id="footer">
    <div style="padding-top: 15px;"></div>
    <div class="container">
        <div class="row">
            <div style="padding-left: 10px;" align="center" class="span22 offset1">
                Copyright &copy; 2013-2014 Ask-Us Q & A System.
                <br/>
                All Rights Reserved.
            </div>
        </div>
    </div>
</div>