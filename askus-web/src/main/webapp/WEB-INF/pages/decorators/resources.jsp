<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script src="<c:url value='/js/jquery/jquery-1.7.2.min.js' />" type="text/javascript"></script>
<script src="<c:url value='/js/bootstrap/bootstrap.js' />" type="text/javascript"></script>
<script src="<c:url value='/js/askus.js' />" type="text/javascript"></script>

<link rel="stylesheet" href="<c:url value='/css/bootstrap.css' />"/>
<link rel="stylesheet" href="<c:url value='/css/bootstrap-custom.css' />"/>
<link rel="stylesheet" href="<c:url value='/css/custom.css' />"/>
<link rel="stylesheet" href="<c:url value='/css/layout.css' />"/>
