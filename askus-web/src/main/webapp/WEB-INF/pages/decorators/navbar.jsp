<%@ page import="com.iit.askus.model.User" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="../../askus-web/home">
                <img src="<c:url value='/img/company-logo.png' />" style="padding-left: 15px;"/>
            </a>

            <sec:authorize ifNotGranted="ROLE_ANONYMOUS">
            <ul class="nav pull-right" style="padding-right: 10px;">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-user icon-white"></i>
                        <% User user = (User) session.getAttribute("user");
                            out.print(user.getFirstName());%>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<c:url value='j_spring_security_logout' />">Sign Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            </sec:authorize>

        </div>
    </div>
</div>