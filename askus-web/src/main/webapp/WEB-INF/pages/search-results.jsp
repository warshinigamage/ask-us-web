<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>

<head>
    <jsp:include page="decorators/resources.jsp"/>
    <script src="<c:url value='/js/jquery/jquery-ui-1.8.16.custom.min.js' />" type="text/javascript"></script>
    <link rel="stylesheet" href="<c:url value='/css/jquery/jquery-ui-1.8.16.custom.css' />"/>
</head>

<body>

<jsp:include page="decorators/navbar.jsp"/>

<div style="background-image: url(<c:url value='/img/bg-mid.png'/>); background-repeat: repeat-y;" class="container"
     id="container-big">
    <div class="row">
        <div class="span22 offset1" style="padding-top: 50px;">

            <%--<div class="row">
                <div class="span22">
                    <c:if test="${not empty model.successNotice}">
                        <div class="alert alert-success" style="text-align: center; margin-bottom: -5px;">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <b>${model.successNotice}</b>
                        </div>
                    </c:if>
                    <c:if test="${not empty model.errorNotice}">
                        <div class="alert alert-error" style="text-align: center; margin-bottom: -5px;">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <b>${model.errorNotice}</b>
                        </div>
                    </c:if>
                </div>
            </div>--%>

            <div class="row" style="padding-top: 10px;">
                <div class="span20 offset1">
                    <div id="liquid-round">
                        <div class="top"><span></span></div>
                        <div class="center-content">
                            <div class="row">
                                <div class="span10 offset1">
                                    <div style="position: relative; z-index: 10">
                                        <h2 class="title-color">
                                            Search Results
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bottom"><span></span></div>
                    </div>
                </div>
                <div class="span5"><span></span></div>
            </div>

        </div>
    </div>
    <div style="padding-top: 80px;"></div>
</div>

<jsp:include page="decorators/footer.jsp"/>


</body>

</html>