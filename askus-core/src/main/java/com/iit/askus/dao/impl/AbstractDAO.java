package com.iit.askus.dao.impl;

import com.iit.askus.dao.IDAO;
import com.iit.askus.model.IPersistable;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public abstract class AbstractDAO implements IDAO {

    private SessionFactory sessionFactory;

    public void save(IPersistable object) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(object);
        session.getTransaction().commit();
    }

    public void delete(IPersistable object) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.delete(object);
        session.getTransaction().commit();
    }

    public void update(IPersistable object) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.update(object);
        session.getTransaction().commit();
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
}
