package com.iit.askus.dao;

import com.iit.askus.model.UserGuardGroup;

import java.util.List;

public interface IUserGuardGroupDAO{

    //Returns all the available guard groups
    List<UserGuardGroup> getAllUserGroups();
}
