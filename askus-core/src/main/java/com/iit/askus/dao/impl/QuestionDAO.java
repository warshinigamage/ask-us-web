package com.iit.askus.dao.impl;

import com.iit.askus.dao.IQuestionDAO;
import com.iit.askus.model.Lesson;
import com.iit.askus.model.Question;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class QuestionDAO extends AbstractDAO implements IQuestionDAO {

    @Autowired
    LessonDAO lessonDAO;

    // Returns a list of question objects using lesson id
    public List<Question> getQuestionsByLessonId(long lessonId) {
        Session session = getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(Question.class);
        criteria.add(Restrictions.eq(Question.PROPERTY_LESSON_ID, lessonId));
        List questionList =  criteria.list();
        session.getTransaction().commit();
        return questionList;
    }

    // Returns a Question object using a question id
    public Question getQuestionByQuestionId(long questionId) {
        Session session = getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(Question.class);
        criteria.add(Restrictions.eq(Question.PROPERTY_QUESTION_ID, questionId));
        criteria.setMaxResults(1);
        Question question = (Question)criteria.uniqueResult();
        session.getTransaction().commit();
        return question;
    }

    // Saves the question to the database
    public void saveQuestion(Question question) {
        save(question);
    }


    /*
    * Search for a Question
    * SubjectId can have values of 'Any' or a particular subject id
    * 'Any' will return questions related to any subject
    * */
    public List<Question> searchQuestion(String searchText, String subjectid) {

        List<Lesson> lessons = lessonDAO.getLessonsBySubjectId(Long.valueOf(subjectid));

        Session session = getSessionFactory().getCurrentSession();
        session.beginTransaction();

        List<Question> questionLst = new ArrayList<Question>();

        if(lessons.isEmpty()) {
            Criteria criteria = session.createCriteria(Question.class);
            criteria.add(Restrictions.like(Question.PROPERTY_QUESTION, "%" + searchText + "%"));
            questionLst.addAll(criteria.list());
        } else {
            for (Lesson lesson : lessons){
                Criteria criteria = session.createCriteria(Question.class);
                criteria.add(Restrictions.like(Question.PROPERTY_QUESTION, "%" + searchText + "%"));
                criteria.add(Restrictions.eq("lesson.lessonId", lesson.getLessonId()));
                questionLst.addAll(criteria.list());
            }
        }
        session.getTransaction().commit();

        return questionLst;
    }
}
