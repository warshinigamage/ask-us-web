package com.iit.askus.dao.impl;

import com.iit.askus.dao.IStudentDAO;
import com.iit.askus.model.Student;
import org.hibernate.Criteria;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

public class StudentDAO extends AbstractDAO implements IStudentDAO {

    // Returns a student object using user id
    public Student getStudentByUserId(int userId){
        Session session = getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(Student.class);
        criteria.add(Restrictions.eq(Student.PROPERTY_USER_ID, userId));
        criteria.setMaxResults(1);
        Student student = (Student)criteria.uniqueResult();
        session.getTransaction().commit();

        return student;
    }
}
