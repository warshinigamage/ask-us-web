package com.iit.askus.dao;

import com.iit.askus.model.Subject;

import java.util.List;

public interface ISubjectDAO {

    // Returns all the subject objects in the database
    public List<Subject> getAllSubjects();

    //Returns a particular subject object using subject id
    public Subject getSubjectBySubjectId(long subjectId);


}
