package com.iit.askus.dao.impl;

import com.iit.askus.dao.ISubjectDAO;
import com.iit.askus.model.Subject;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class SubjectDAO extends AbstractDAO implements ISubjectDAO{

    // Returns all the subject objects in the database
    @Override
    public List<Subject> getAllSubjects() {
        Session session = getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(Subject.class);
        List subjectList =  criteria.list();
        session.getTransaction().commit();
        return subjectList;
    }

    //Returns a particular subject object using subject id
    public Subject getSubjectBySubjectId(long subjectId) {
        Session session = getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(Subject.class);
        criteria.add(Restrictions.eq(Subject.PROPERTY_SUBJECT_ID, subjectId));
        criteria.setMaxResults(1);
        Subject subject = (Subject)criteria.uniqueResult();
        session.getTransaction().commit();
        return subject;
    }
}
