package com.iit.askus.dao.impl;

import com.iit.askus.dao.ILessonDAO;
import com.iit.askus.model.Lesson;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class LessonDAO extends AbstractDAO implements ILessonDAO {

    // Returns a list of Lesson objects using subject id
    public List<Lesson> getLessonsBySubjectId(long subjectId) {
        Session session = getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(Lesson.class);
        criteria.add(Restrictions.eq(Lesson.PROPERTY_SUBJECT_ID, subjectId));
        List lessonList =  criteria.list();
        session.getTransaction().commit();
        return lessonList;
    }

    // Returns a Lesson object using a Lesson Id
    public Lesson getLessonByLessonId(long lessonId) {
        Session session = getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(Lesson.class);
        criteria.add(Restrictions.eq(Lesson.PROPERTY_LESSON_ID, lessonId));
        criteria.setMaxResults(1);
        Lesson lesson = (Lesson)criteria.uniqueResult();
        session.getTransaction().commit();
        return lesson;
    }

}
