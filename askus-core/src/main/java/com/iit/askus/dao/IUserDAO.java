package com.iit.askus.dao;

import com.iit.askus.model.User;

import java.util.List;
import java.util.Map;

public interface IUserDAO {

    //Returns a User object using user id
    public User getUserById(int userId);

    //Returns a User object using username
    public User getUserByUsername(String userName);

    //Saves the user to the database
    public void saveUser(User user);
}
