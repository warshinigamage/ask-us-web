package com.iit.askus.dao.impl;

import com.iit.askus.dao.IAnswerDAO;
import com.iit.askus.model.Answer;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class AnswerDAO extends AbstractDAO implements IAnswerDAO {

    //    Returns a list of Answer objects using question id
    public List<Answer> getAnswersByQuestionId(long questionId) {
        Session session = getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(Answer.class);
        criteria.add(Restrictions.eq(Answer.PROPERTY_QUESTION_ID, questionId));
        List answerList =  criteria.list();
        session.getTransaction().commit();
        return answerList;
    }

    //    Saves the Answer to the database
    public void saveAnswer(Answer answer) {
        save(answer);
    }
}
