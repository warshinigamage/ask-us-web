package com.iit.askus.dao.impl;

import com.iit.askus.dao.IUserDAO;
import com.iit.askus.model.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

public class UserDAO extends AbstractDAO implements IUserDAO {

    //Returns a User object using user id
    public User getUserById(int userId){
        Session session = getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(User.class);
        criteria.add(Restrictions.eq(User.PROPERTY_USER_ID, userId));
        criteria.setMaxResults(1);
        User user = (User)criteria.uniqueResult();
        session.getTransaction().commit();

        return user;
    }

    //Returns a User object using username
    public User getUserByUsername(String userName){
        Session session = getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(User.class);
        criteria.add(Restrictions.eq(User.PROPERTY_USER_NAME, userName));
        criteria.setMaxResults(1);
        User user = (User)criteria.uniqueResult();
        session.getTransaction().commit();

        return user;
    }

    //Saves the user to the database
    public void saveUser(User user) {
        save(user);
    }


}
