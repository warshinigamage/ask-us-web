package com.iit.askus.dao.impl;

import com.iit.askus.dao.IUserGuardGroupDAO;
import com.iit.askus.model.UserGuardGroup;
import org.hibernate.Criteria;
import org.hibernate.Session;

import java.util.List;

public class UserGuardGroupDAO extends AbstractDAO implements IUserGuardGroupDAO {

    //Returns all the available guard groups
    @Override
    public List<UserGuardGroup> getAllUserGroups(){
        Session session = getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(UserGuardGroup.class);
        List userGroupList = criteria.list();
        session.getTransaction().commit();
        return userGroupList;
    }
}
