package com.iit.askus.dao;

import com.iit.askus.model.IPersistable;

public interface IDAO {

    public void save(IPersistable object);

    public void delete(IPersistable object);

    public void update(IPersistable object);
}
