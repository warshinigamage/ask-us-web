package com.iit.askus.dao.impl;

import com.iit.askus.dao.IStudentDAO;
import com.iit.askus.dao.ITutorDAO;
import com.iit.askus.dao.impl.AbstractDAO;
import com.iit.askus.model.Student;
import com.iit.askus.model.Tutor;
import com.iit.askus.model.TutorStatus;
import org.hibernate.Criteria;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class TutorDAO extends AbstractDAO implements ITutorDAO {

    //Returns a Tutor object using user id
    public Tutor getTutorByUserId(int userId){
        Session session = getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(Tutor.class);
        criteria.add(Restrictions.eq(Tutor.PROPERT_USER_ID, userId));
        criteria.setMaxResults(1);
        Tutor tutor = (Tutor)criteria.uniqueResult();
        session.getTransaction().commit();

        return tutor;
    }

    //Returns a list of tutor obejects that has the status PENDING APPROVAL
    public List<Tutor> getPendingTutors() {
        Session session = getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(Tutor.class);
        criteria.add(Restrictions.eq(Tutor.PROPERT_TUTOR_STATUS, TutorStatus.PENDING_APPROVAL));
        List tutorList =  criteria.list();
        session.getTransaction().commit();
        return tutorList;
    }
}
