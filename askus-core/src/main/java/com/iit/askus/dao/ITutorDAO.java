package com.iit.askus.dao;

import com.iit.askus.model.Tutor;

import java.util.List;

public interface ITutorDAO {

    //Returns a Tutor object using user id
    public Tutor getTutorByUserId(int userId);

    //Returns a list of tutor obejects that has the status PENDING APPROVAL
    public List<Tutor> getPendingTutors();
}
