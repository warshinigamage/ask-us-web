package com.iit.askus.dao;

import com.iit.askus.model.Lesson;
import java.util.List;

public interface ILessonDAO {

    // Returns a list of Lesson objects using subject id
    public List<Lesson> getLessonsBySubjectId(long subjectId);

    // Returns a Lesson object using a Lesson Id
    public Lesson getLessonByLessonId(long lessonId);
}
