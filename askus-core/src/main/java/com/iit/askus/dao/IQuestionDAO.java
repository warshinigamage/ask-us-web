package com.iit.askus.dao;

import com.iit.askus.model.Question;

import java.util.List;

public interface IQuestionDAO {

    // Returns a list of question objects using lesson id
    public List<Question> getQuestionsByLessonId(long lessonId);

    // Returns a Question object using a question id
    public Question getQuestionByQuestionId(long questionId);

    // Saves the question to the database
    public void saveQuestion(Question question);

    /*
    * Search for a Question
    * SubjectId can have values of 'Any' or a particular subject id
    * 'Any' will return questions related to any subject
    * */
    public List<Question> searchQuestion(String searchText, String subjectName);
}
