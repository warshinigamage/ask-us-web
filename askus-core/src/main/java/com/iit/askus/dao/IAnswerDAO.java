package com.iit.askus.dao;

import com.iit.askus.model.Answer;

import java.util.List;

public interface IAnswerDAO {

    //    Returns a list of Answer objects using question id
    public List<Answer> getAnswersByQuestionId(long questionId);

    //    Saves the Answer to the database
    public void saveAnswer(Answer answer);

}
