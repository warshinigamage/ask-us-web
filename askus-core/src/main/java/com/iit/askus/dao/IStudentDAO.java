package com.iit.askus.dao;

import com.iit.askus.model.Student;

public interface IStudentDAO {

    // Returns a student object using user id
    public Student getStudentByUserId(int userId);
}
