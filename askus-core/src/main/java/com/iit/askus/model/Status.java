package com.iit.askus.model;

public enum Status {
    ENABLED, DISABLED, DELETED
}
