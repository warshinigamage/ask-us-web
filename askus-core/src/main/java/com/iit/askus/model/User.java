package com.iit.askus.model;

import org.codehaus.jackson.annotate.JsonBackReference;

import java.sql.Date;
import java.util.Set;

/**
 * Domain Object to hold user instance.
 */
public class User implements IPersistable{

    public static final String PROPERTY_USER_ID = "userId";
    public static final String PROPERTY_USER_NAME = "userName";
    public static final String PROPERTY_FIRST_NAME = "firstName";
    public static final String PROPERTY_LAST_NAME = "lastName";
    public static final String ROLE_ADMIN = "ROLE_ADMIN_GROUP";

    private int userId;
    private String userName;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private String password;
    private Gender gender;
    private UserGuardGroup userGuardGroup;
    @JsonBackReference private Student student;
    private Tutor tutor;
    private String nicNo;
    private String email;
    private boolean enabled = true;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public UserGuardGroup getUserGuardGroup() {
        return userGuardGroup;
    }

    public void setUserGuardGroup(UserGuardGroup userGuardGroup) {
        this.userGuardGroup = userGuardGroup;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Tutor getTutor() {
        return tutor;
    }

    public void setTutor(Tutor tutor) {
        this.tutor = tutor;
    }

    public String getNicNo() {
        return nicNo;
    }

    public void setNicNo(String nicNo) {
        this.nicNo = nicNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
