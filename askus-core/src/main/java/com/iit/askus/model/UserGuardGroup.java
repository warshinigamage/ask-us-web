package com.iit.askus.model;

/**
 * Domain Object to hold guard group instance.
 */

public class UserGuardGroup {
    
    private int userGroupId;
    private String userGroupName;

    public int getUserGroupId() {
        return userGroupId;
    }

    public void setUserGroupId(int userGroupId) {
        this.userGroupId = userGroupId;
    }

    public String getUserGroupName() {
        return userGroupName;
    }

    public void setUserGroupName(String userGroupName) {
        this.userGroupName = userGroupName;
    }
}
