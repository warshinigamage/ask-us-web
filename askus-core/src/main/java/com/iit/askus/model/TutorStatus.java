package com.iit.askus.model;


public enum TutorStatus {
    APPROVED, PENDING_APPROVAL, SUSPENDED;

}
