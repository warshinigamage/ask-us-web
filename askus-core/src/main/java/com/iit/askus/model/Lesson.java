package com.iit.askus.model;

/**
 * Domain Object to hold lesson instance
 */
public class Lesson implements IPersistable {

    private static final long serialVersionUID = -1898923960055617501L;
    public static final String PROPERTY_SUBJECT_ID="subject.subjectId";
    public static final String PROPERTY_LESSON_ID="lessonId";
    private long lessonId;
    private String lessonName;
    private Subject subject;

    public long getLessonId() {
        return lessonId;
    }

    public void setLessonId(long lessonId) {
        this.lessonId = lessonId;
    }

    public String getLessonName() {
        return lessonName;
    }

    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

}
