package com.iit.askus.model;

/**
 * Domain Object to hold answer instance.
 */
public class Answer implements IPersistable, Comparable {

    private static final long serialVersionUID = 7170356324073482753L;
    public static final String PROPERTY_QUESTION_ID = "question.questionId";
    private long answerId;
    private String answer;
    private Question question;
    private Tutor tutor;
    private int RateSum;
    private int rateCount;

    public long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(long answerId) {
        this.answerId = answerId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Tutor getTutor() {
        return tutor;
    }

    public void setTutor(Tutor tutor) {
        this.tutor = tutor;
    }

    public int getRateSum() {
        return RateSum;
    }

    public void setRateSum(int rateSum) {
        RateSum = rateSum;
    }

    public int getRateCount() {
        return rateCount;
    }

    public void setRateCount(int rateCount) {
        this.rateCount = rateCount;
    }

    @Override
    public int compareTo(Object o) {
        Answer answerObj = (Answer) o;
        final int BEFORE = -1;
        final int EQUAL = 0;
        final int AFTER = 1;

        double thisRating = this.getRateSum()* 1.0/this.getRateCount();
        double otherRating = answerObj.getRateSum()* 1.0/answerObj.getRateCount();

        if(thisRating == otherRating) {
            return EQUAL;
        } else if (thisRating > otherRating) {
            return BEFORE;
        } else return AFTER;
    }

}
