package com.iit.askus.model;

import org.codehaus.jackson.annotate.JsonManagedReference;

/**
 * Domain Object to hold studet instance.
 */
public class Student implements IPersistable {

    public static final String PROPERTY_USER_ID = "userId";
    public static final String PROPERTY_SCHOOL = "STUDENT";
    public static final String PROPERTY_YEAR_OF_AL = "EXAM_YEAR";

    private int userId;
    private String school;
    private String examYear;
    @JsonManagedReference private User user;

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getExamYear() {
        return examYear;
    }

    public void setExamYear(String examYear) {
        this.examYear = examYear;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
