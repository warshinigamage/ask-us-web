package com.iit.askus.model;

/**
 * Domain object to hold Subject Instances.
 */
public class Subject implements IPersistable {

    public static final String PROPERTY_SUBJECT_ID="subjectId";

    private long subjectId;
    private String subjectName;

    public long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(long subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

}
