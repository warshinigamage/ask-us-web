package com.iit.askus.model;

/**
 * Domain Object to hold Gender instance.
 */
public enum Gender {
    M("Male"), F("Female");
    
    String value;

    private Gender(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
