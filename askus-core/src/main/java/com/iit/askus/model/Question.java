package com.iit.askus.model;

/**
 * Domain object to hold question instances.
 */
public class Question implements IPersistable, Comparable {

    private static final long serialVersionUID = 5835774133766467230L;
    public static final String PROPERTY_LESSON_ID="lesson.lessonId";
    public static final String PROPERTY_QUESTION_ID="questionId";
    public static final String PROPERTY_QUESTION="question";
    public static final String PROPERTY_SUBJECT_NAME ="lesson.subject.subjectId";
    private long questionId;
    private String question;
    private Student student;
    private Lesson lesson;
    private int RateSum;
    private int rateCount;


    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public int getRateSum() {
        return RateSum;
    }

    public void setRateSum(int rateSum) {
        RateSum = rateSum;
    }

    public int getRateCount() {
        return rateCount;
    }

    public void setRateCount(int rateCount) {
        this.rateCount = rateCount;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    @Override
    public int compareTo(Object o) {
        Question questionObj = (Question) o;
        final int BEFORE = -1;
        final int EQUAL = 0;
        final int AFTER = 1;

        double thisRating = this.getRateSum()* 1.0/this.getRateCount();
        double otherRating = questionObj.getRateSum()* 1.0/questionObj.getRateCount();

        if(thisRating == otherRating) {
            return EQUAL;
        } else if (thisRating > otherRating) {
            return BEFORE;
        } else return AFTER;
    }
}
