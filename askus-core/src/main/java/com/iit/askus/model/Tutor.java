package com.iit.askus.model;

/**
 * Domain Object to hold tutor instance.
 */
public class Tutor {

    public static final String PROPERT_USER_ID = "userId";
    public static final String PROPERT_TUTOR_STATUS = "tutorStatus";
    private int userId;
    private String skypeId;
    private Subject qualifiedSubject;
    private String educationalQualifications;
    private String professionalQualifications;
    private TutorStatus tutorStatus;
    private User user;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getSkypeId() {
        return skypeId;
    }

    public void setSkypeId(String skypeId) {
        this.skypeId = skypeId;
    }

    public Subject getQualifiedSubject() {
        return qualifiedSubject;
    }

    public void setQualifiedSubject(Subject qualifiedSubject) {
        this.qualifiedSubject = qualifiedSubject;
    }

    public String getEducationalQualifications() {
        return educationalQualifications;
    }

    public void setEducationalQualifications(String educationalQualifications) {
        this.educationalQualifications = educationalQualifications;
    }

    public String getProfessionalQualifications() {
        return professionalQualifications;
    }

    public void setProfessionalQualifications(String professionalQualifications) {
        this.professionalQualifications = professionalQualifications;
    }

    public TutorStatus getTutorStatus() {
        return tutorStatus;
    }

    public void setTutorStatus(TutorStatus tutorStatus) {
        this.tutorStatus = tutorStatus;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
