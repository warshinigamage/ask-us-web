use askus;

insert into user_guard_group values(1, "ROLE_ADMIN_GROUP");
insert into user_guard_group values(2, "ROLE_TUTOR_GROUP");
insert into user_guard_group values(3, "ROLE_STUDENT_GROUP");

insert into user values(1, "admin", "John","Doe","2013-12-18", "44bc7b417faef5c8125581ebaf8b8e1e", "9409876V", "test@iit.lk", 0, 1, true);
insert into user values(2, "tutor1", "Steve","Jay","2013-12-18", "44bc7b417faef5c8125581ebaf8b8e1e", "9409876V", "test@iit.lk", 0, 2, true);
insert into user values(3, "student1", "Adam","Scott","2013-12-18", "44bc7b417faef5c8125581ebaf8b8e1e", "9409876V", "test@iit.lk", 0, 3, true);
insert into user values(4, "student2", "James","Oliver","2013-12-18", "44bc7b417faef5c8125581ebaf8b8e1e", "9409876V", "test@iit.lk", 0, 3, true);
insert into user values(5, "student3", "Peter","Parker","2013-12-18", "44bc7b417faef5c8125581ebaf8b8e1e", "9409876V", "test@iit.lk", 0, 3, true);
insert into user values(6, "tutor2", "Sally","Field","2013-12-18", "44bc7b417faef5c8125581ebaf8b8e1e", "9409876V", "test@iit.lk", 0, 2, true);
insert into user values(7, "tutor3", "Baxter","Collins","2013-12-18", "44bc7b417faef5c8125581ebaf8b8e1e", "9409876V", "test@iit.lk", 0, 2, true);
insert into user values(8, "tutor4", "Maggie","Sue","2013-12-18", "44bc7b417faef5c8125581ebaf8b8e1e", "9409876V", "test@iit.lk", 0, 2, true);
insert into user values(9, "tutor5", "Sean","Peet","2013-12-18", "44bc7b417faef5c8125581ebaf8b8e1e", "9409876V", "test@iit.lk", 0, 2, true);

insert into student values(3, "Panadura Sumangala College", "2014");
insert into student values(4, "Royal College", "2014");
insert into student values(5, "St Thomas College", "2014");

insert into subject values(1, "Biology");
insert into subject values(2, "Physics");
insert into subject values(3, "Chemistry");

insert into lesson values(1, "Introduction to Biology", 1);
insert into lesson values(2, "Chemical Basis of Life", 1);
insert into lesson values(3, "Diversity of Organisms", 1);
insert into lesson values(4, "Nutrition", 1);
insert into lesson values(5, "Respiration", 1);
insert into lesson values(6, "Transportation", 1);
insert into lesson values(7, "Coordination and Homeostasis", 1);
insert into lesson values(8, "Reproduction, Growth and Development", 1);
insert into lesson values(9, "Physical Quantities and Dimensions", 2);
insert into lesson values(10, "Mechanics", 2);
insert into lesson values(11, "Oscillations and Waves", 2);
insert into lesson values(12, "Thermal Physics", 2);
insert into lesson values(13, "Fields", 2);
insert into lesson values(14, "Electricity", 2);
insert into lesson values(15, "Matter and Radiation", 2);
insert into lesson values(16, "Mechanical Properties of Matter", 2);
insert into lesson values(17, "Electronics", 2);
insert into lesson values(18, "General Chemistry", 3);
insert into lesson values(19, "Organic Chemistry", 3);
insert into lesson values(20, "Inorganic Chemistry", 3);
insert into lesson values(21, "Physical Chemistry", 3);
insert into lesson values(22, "Industrial Chemistry", 3);

insert into tutor values(2, "steve22", "BSc in Biology", "Biology Teacher at Royal College (2011-2013)", 0, 1);
insert into tutor values(6, "sally55", "BSc in Chemistry", "Chemistry Teacher at Joseph College (2012-2013)", 0, 3);
insert into tutor values(7, "baxmax", "BSc in Physics", "Physics Teacher at Thomas College (2002-2013)", 0, 2);
insert into tutor values(8, "maggie67", "Phd in Biology", "Biology Lecturer at Cambridge University (2002-2013)", 0, 1);
insert into tutor values(9, "seanrox", "Phd in Biology", "Biology Lecturer at Southampton University (2005-2019)", 0, 1);

insert into question values(1, "What is DNA?", 4, 8, 75, 22);
insert into question values(2, "How is testesterone created?", 3, 8, 103, 24);
insert into question values(3, "What is estrogen?", 4, 8, 18, 5);
insert into question values(4, "How is electricity measured?", 5, 14, 155, 52);
insert into question values(5, "What is the 3rd element in the periodic table?", 5, 20, 850, 315);
insert into question values(6, "Question with insert keyword", 5, 14, 111, 52);
insert into question values(7, "Question with delete word", 5, 20, 778, 315);

insert into answer values(1, "answer one", 2, 1, 88, 30);
insert into answer values(2, "answer two", 8, 1, 345, 88);
insert into answer values(3, "answer three", 9, 1, 45, 12);
insert into answer values(4, "some answer", 7, 4, 85, 33);
insert into answer values(5, "some other answer", 6, 5, 155, 42);

